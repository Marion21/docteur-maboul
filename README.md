Projet :

Vous devez réaliser une page web, contenant différentes parties du corps et leur description au click sur celle-ci.

Contraintes :

Avant tout, ce projet est bien-sur à réaliser seul.

Vous devez reproduire la maquette que le designer a réalisé.

En plus de:

● Utilisation des attributs HTML “class” et “id”

● Utilisation de la police Arvo (voir sur GoogleFont)

● Utilisation des balises et attributs HTML sémantiques

● Séparation du HTML, du CSS et du JavaScript

Prérequis : Aucun.

Informations additionnelles :

Vous pouvez bien évidemment choisir la couleur de fond.
